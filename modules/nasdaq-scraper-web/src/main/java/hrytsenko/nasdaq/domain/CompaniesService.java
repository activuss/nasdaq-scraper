package hrytsenko.nasdaq.domain;

import java.util.List;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.inject.Inject;

import hrytsenko.nasdaq.domain.data.Company;

@Stateless
public class CompaniesService {

    @Inject
    private CompaniesRepository companiesRepository;

    public List<Company> findCompanies(Optional<String> exchange, Optional<String> symbol, Optional<String> sector) {
        return companiesRepository.findCompanies(exchange, symbol, sector);
    }

    public void updateCompany(Company company) {
        companiesRepository.updateCompany(company);
    }

    public List<String> findSectors() {
        return companiesRepository.findSectors();
    }

}
