package hrytsenko.nasdaq.client.data;

import hrytsenko.nasdaq.domain.data.Company;

public class ClientCompany {

    private final String exchange;
    private final String symbol;
    private final String name;

    private final String sector;
    private final String subsector;

    public ClientCompany(String exchange, String symbol, String name, String sector, String subsector) {
        this.exchange = exchange;
        this.symbol = symbol;
        this.name = name;
        this.sector = sector;
        this.subsector = subsector;
    }

    public static ClientCompany fromCompany(Company company) {
        return new ClientCompany(company.getExchange(), company.getSymbol(), company.getName(), company.getSector(),
                company.getSubsector());
    }

    public String getExchange() {
        return exchange;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getName() {
        return name;
    }

    public String getSector() {
        return sector;
    }

    public String getSubsector() {
        return subsector;
    }

}
