package hrytsenko.nasdaq.client;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.google.common.base.Strings;

import hrytsenko.nasdaq.client.data.ClientCompany;
import hrytsenko.nasdaq.domain.CompaniesService;

@Stateless
@Path("companies")
@Produces(MediaType.APPLICATION_JSON)
public class CompaniesRestService {

    @Inject
    private CompaniesService companiesService;

    @GET
    public List<ClientCompany> findCompanies(@QueryParam("exchange") String exchange,
            @QueryParam("symbol") String symbol, @QueryParam("sector") String sector) {
        return companiesService.findCompanies(optionalOf(exchange), optionalOf(symbol), optionalOf(sector)).stream()
                .map(ClientCompany::fromCompany).collect(Collectors.toList());
    }

    private Optional<String> optionalOf(String parameter) {
        return Optional.ofNullable(Strings.emptyToNull(parameter));
    }

    @GET
    @Path("sectors")
    public List<String> findSectors() {
        return companiesService.findSectors();
    }

}
